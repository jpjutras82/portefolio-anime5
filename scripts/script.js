function scrollAppear(){
    var element_selected = document.querySelector('.element_hidden');
    var scrollPosition = element_selected.getBoundingClientRect().top;
    var screenPosition = window.innerHeight / 1.3;
    if(scrollPosition < screenPosition){
        element_selected.classList.add('element_appear')
    }
}

window.addEventListener('scroll',scrollAppear);



// get Formulaire
var modal = document.getElementById("formulaire_infolettre");

// Get bouton Inscrivez-vous...
var btn = document.getElementById("bouton_infolettre");

// Get bouton close
var span = document.getElementsByClassName("close")[0];

// lorsque clic, ouvrir le formulaire
btn.onclick = function() {
  modal.style.display = "block";
}

// lorsque clique sur bouton Close, fermer formulaire
span.onclick = function() {
  modal.style.display = "none";
}

// lorsque clique à l'extérieur du formulaire, fermer celui-ci
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}